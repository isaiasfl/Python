# -*- coding: utf-8 -*-
#       Para poner comentarios de una línea, usamos la --> #
'''
        Para comentarios de más de una
        línea, usaremos tres comillas simples antes y al final o las comillas dobles tres veces
'''

"""
        Existen variables que hay que poner en todos los programas en python. Son el author y un comentario
        de qué hace este programa.
        
@author: Isaías Fernández Lozano.
@comment: Programa que me pide datos por pantalla y muestra información.
        Recuerdo que raw_input() se utiliza para capturar lo que escribimos por pantalla.
"""
#        Para declarar una variable usaremos la siguiente sintaxis:
#        nombrevariable = valordelavariable
edad = 10       #muy importantes son los espacios. Esta es una variable entera. Esta variables des de tipo int.
peso = 25.5     # para valores decimales usaremos el punto. Automáticamente esa variable es de tipo float.
nombre = "Antonio" # para declarar variables de tipo texto usaremos las dobles comillas. Esta variables es de tipo str (de String)
permiso = 's' #para declarar variables char o de un sólo carácter usamos comillas simples. Esta variable es de tipo chr.

#Para pedir datos por teclado usaremos la función raw_input("aquí vendría el texto que mostrar por pantalla")
apellidos = raw_input("Introduzca sus apellidos: ")  #almacenamos en la variable apellidos lo que introducimos por pantalla.
# Recordar que raw_input captura lo que escribimos como si fuese texto o String (str).
# Para mostrar algo por pantalla usaremos el comando print. 
# Print imprime sólo texto. Si quiero mostrar una variable que es numérica tengo que convertirla a texto
# englobándola con la función str(aquí la variable numérica).

print "Bienvenido D. " + nombre + " " + apellidos
print "Usted tiene " + str(edad) + " años de edad."

# if se utiliza para tomar decisiones. la estructura sería:
'''
if condicion:
    acciones si la condición es verdad (recuerda que lleva tabulador)
    
else:
    condiciones si la condición es falsa. (recuerda que lleva tabulador)

Para salir del if basta quitar el tabulador.
'''
if edad > 18:
    print "D. " + nombre + " usted es mayor de edad."
else:
    print "D. " + nombre +  " usted no es mayor de edad"

print "Finalizando la aplicación ...."


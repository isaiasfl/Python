# -*- coding: utf-8 -*-
'''
@author: Isaías Fernández Lozano.
@comment: Programa que permite realizar una simple calculadora en python.
        Para ello me pedirá dos números.
        Seguidamente nos pedirá una de las posibles operaciones a realizar (+,-,*,/).
        Por último mostrará la operación y la solución a la misma.
'''

# -*- coding: utf-8 -*-
'''
@author: Isaías Fernández Lozano.
@comment: Programa que permite calcular el área de un triángulo.
        Para ello me pedirá dos datos (El lado y la altura) y automáticamente calculará el área de un triángulo
        mostrando por pantalla el resultado.
        Recuerdo que raw_input() se utiliza para capturar lo que escribimos por pantalla.
'''
'''
    ----------------- INFORMACIÓN ADICIONAL DE PYTHON -------------------------
    
    Operadores de comparación con cadenas de texto.
    Para comparar cadenas de texto usaremos:
    textoA == textoB <-- devuelve verdadero si el contenido de la variable textoA es igual que el contenido de la variable textoB
    textoA != textoB <-- devuelve verdadero si el contenido de la variable textoA es distingo que el contenido de la variable textoB

    Para comparar números usaremos:
    numero1 > numero2 <-- devuelve verdadero si el contenido de la variable numero1 es mayor numéricamente hablando que el contenido de la variable numero2
    numero1 >= numero2 <-- mayor o igual.
    numero1 < numero2 <-- menor que
    numero1 <= numero2 <-- menor o igual
    numero1 == numero2 <-- igual
    
    Operadores para realizar operaciones:
    +   <-- suma
    -   <-- resta
    *   <-- multiplicación
    /   <-- división
    //  <-- división entera
    %   <-- módulo o resto de la división
    
    Estructura de control: Condicional IF
    if condicion:
        si es verdad 
    else:
        si no es verdad
        
    Condicional IF anidado
    if condicion1:
        acciones si es verdar la condicion1
    elif condicion2:
        accciones si es verdad la condicion2
    elif condicion3:
        acciones si es verdad la condicion3
    else:
        acciones si no es verdad ninguna de las condiciones anteriores
    
    Operadores and // or 
    
    and simbolia el Y lógico
    or simboliza el O lógico
    
    Ejemplo:
        if edad <= 10 or edad >= 18:
            print "Edad no válida para el carnet joven"

'''
# -*- coding: utf-8 -*-
'''
@author: Isaías Fernández Lozano.
@comment: Programa que permite calcular el área una figura geométrica.
        Para ello me preguntará qué figura quiero calcular el área. Las posibles figuras serán: Cuadrado, Rectángulo, Triangulo, Círculo.
        Según la elección, me pedirá los datos correspondientes según la figura.
        Por último mostrará mensaje con el resultado.
'''